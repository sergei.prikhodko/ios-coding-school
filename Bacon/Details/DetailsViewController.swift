//
//  DetailsViewController.swift
//  Bacon
//
//  Created by Sergei Prikhodko on 05.03.21.
//

import UIKit

class DetailsViewController: UIViewController {

    let viewModel: DetailsViewModel

    @IBOutlet weak var imageView: UIImageView!

    init(viewModel: DetailsViewModel) {
        self.viewModel = viewModel

        super.init(nibName: "DetailsViewController", bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = viewModel.liveStream.title
        view.backgroundColor = UIColor.yellow
        imageView.tintColor = .lightGray
        fetchLogo()

    }

    private func fetchLogo() {
        viewModel.fetchLogo { data in
            DispatchQueue.main.async {
                if let data = data {
                    self.imageView.image = UIImage(data: data)?.withRenderingMode(.alwaysTemplate)
                } else {
                    // fallback image
                }

            }
        }
    }

}
