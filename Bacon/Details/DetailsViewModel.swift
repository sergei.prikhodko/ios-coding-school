//
//  DetailsViewModel.swift
//  Bacon
//
//  Created by Sergei Prikhodko on 19.03.21.
//

import Foundation

final class DetailsViewModel {

    let liveStream: LiveStream

    init(liveStream: LiveStream) {
        self.liveStream = liveStream
    }

    func fetchLogo(_ completion: @escaping ((Data?) -> Void)) {
        guard let logoUrl = liveStream.brand?.logo?.url else {
            return
        }

        let urlString = logoUrl + "/profile:nextgen-iosphone-artlogo-440x180"
        NetworkManager().fetchImage(urlString, completion: { data in
            completion(data)
        })
    }

}
