//
//  HomeViewController.swift
//  Bacon
//
//  Created by Sergei Prikhodko on 26.02.21.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    let viewModel = HomeViewModel()

    // MARK: LifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        title = viewModel.title

        let barButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.refresh, target: self, action: #selector(refreshTapped))
        navigationItem.rightBarButtonItem = barButtonItem

        refreshTapped()
    }

    // MARK: IBAction

    @objc func refreshTapped() {
        viewModel.requestFeelsLike { feelsLike in
            DispatchQueue.main.async {
                if let feelsLike = feelsLike {
                    self.title = "Feels like \(feelsLike)"
                }
            }
        }

        viewModel.requestLiveStreams {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }

}

extension HomeViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.livestreams?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellId") ?? UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: "CellId")

        cell.tintColor = .red
        cell.textLabel?.text = viewModel.livestreams?[indexPath.row].title
        cell.detailTextLabel?.text = viewModel.livestreams?[indexPath.row].epg?.first?.title
        cell.imageView?.image = UIImage(named: "live")

        return cell
    }

}

extension HomeViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let detailsViewModel = viewModel.detailsViewModel(for: indexPath.row) {
            let detailsViewController = DetailsViewController(viewModel: detailsViewModel)
            navigationController?.pushViewController(detailsViewController, animated: true)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }

}
