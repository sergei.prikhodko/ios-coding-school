//
//  HomeViewModel.swift
//  Bacon
//
//  Created by Sergei Prikhodko on 12.03.21.
//

import Foundation

class HomeViewModel {

    let networkManager = NetworkManager()

    let title = "Home from VM"

    var livestreams: [LiveStream]?

    func requestFeelsLike(_ completion: @escaping ((Float?) -> Void)) {
        networkManager.requestWeather { weather in
            let feelsLike = weather?["feels_like"] as? NSNumber
            completion(feelsLike?.floatValue)
        }
    }

    func requestLiveStreams(_ completion: @escaping (() -> Void)) {
        networkManager.requestLiveStreams { livestreams in
            self.livestreams = livestreams
            completion()
        }
    }

    func detailsViewModel(for liveStreamIndex: Int) -> DetailsViewModel? {
        guard let livestream = livestreams?[liveStreamIndex] else { return nil }

        return DetailsViewModel(liveStream: livestream)
    }

}
