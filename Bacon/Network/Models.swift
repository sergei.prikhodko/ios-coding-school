//
//  Models.swift
//  Bacon
//
//  Created by Sergei Prikhodko on 12.03.21.
//

import Foundation

struct BaseModel: Decodable {
    let data: GQLData?
}

struct GQLData: Decodable {
    let liveStreams: [LiveStream]?
}

struct LiveStream: Decodable {
    let title: String?
    let epg: [EPG]?
    let brand: Brand?
}

struct EPG: Decodable {
    let title: String?
}

struct Brand: Decodable {
    let id: String?
    let logo: Logo?
}

struct Logo: Decodable {
    let url: String?
}
