//
//  NetworkManager.swift
//  Bacon
//
//  Created by Sergei Prikhodko on 12.03.21.
//

import Foundation

enum K {

    static let weatherURL = "https://api.openweathermap.org/data/2.5/weather?q=Munich&units=metric&appid=d2e6d56e96094411469cbe5a3a591da9"

    static let joynURL = "https://api.joyn.de/graphql"

    static var liveStreamsPayload: Data? = {
        let rawQuery = """
        {
          liveStreams {
            title
            epg {
              title
            }
            brand {
              id
              logo {
                url
              }
            }
          }
        }
        """
        let parameters = ["query": rawQuery] as [String: Any]

        return try? JSONSerialization.data(withJSONObject: parameters, options: [])
    }()

}

final class NetworkManager {

    func requestWeather(_ completion: @escaping (([String: Any]?) -> Void)) {
        guard let url = URL(string: K.weatherURL) else { return }

        let session = URLSession.shared.dataTask(with: url, completionHandler: { [weak self] data, _, error in
            if error == nil, let data = data {
                completion(self?.parseResponse(data))
            }
        })

        session.resume()
    }

    func requestLiveStreams(_ completion: @escaping (([LiveStream]?) -> Void)) {
        guard let url = URL(string: K.joynURL) else { return }

        var request = URLRequest(url: url)
        request.addValue("390a9516d4dc88f806ce7c7269f71c23", forHTTPHeaderField: "X-Api-Key")
        request.addValue("iOS", forHTTPHeaderField: "Joyn-Platform")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        request.httpBody = K.liveStreamsPayload

        let session = URLSession.shared.dataTask(with: request, completionHandler: { data, _, error in
            if error == nil, let data = data {
                let responseModel = try? JSONDecoder().decode(BaseModel.self, from: data)
                completion(responseModel?.data?.liveStreams)
            }
        })

        session.resume()
    }

    func fetchImage(_ urlString: String?, completion: @escaping ((Data?) -> Void)) {
        guard let urlString = urlString,
              let url = URL(string: urlString) else { return completion(nil) }

        DispatchQueue.global().async {
            let data = try? Data(contentsOf: url)
            completion(data)
        }
    }

}

private extension NetworkManager {

    func parseResponse(_ data: Data) -> [String: Any]? {
        let json = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]
        let main = json?["main"] as? [String: Any]
        return main
    }

}
