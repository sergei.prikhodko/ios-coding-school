//
//  SceneDelegate.swift
//  Bacon
//
//  Created by Sergei Prikhodko on 26.02.21.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {

        if let windowScene = (scene as? UIWindowScene) {
            let window = UIWindow(windowScene: windowScene)
            let homeViewController = HomeViewController()

            let navigation = UINavigationController(rootViewController: homeViewController)
            window.rootViewController = navigation

            self.window = window
            window.makeKeyAndVisible()
        }

    }

}

